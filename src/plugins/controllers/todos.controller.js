async function controller (f, options, done) {
  f.get('/', async (request, reply) => {
    return { content: await f.domain.Todo.find() }
  })

  f.get('/:id', async (request, reply) => {
    return { content: await f.domain.Todo.findById(request.params.id) }
  })

  f.post('/', async (request, reply) => {
    reply.code(201) // TODO: Refactor
    return { content: await f.domain.Todo.create(request.body) }
  })

  f.delete('/:id', async (request, reply) => {
    return { content: await f.domain.Todo.findById(request.params.id) }
  })

  done()
}

module.exports = controller
