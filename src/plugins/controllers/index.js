/**
 * A plugin that provide encapsulated routes
 * @param {FastifyInstance} f encapsulated fastify instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
async function controllers (f, options, done) {
  f.register(require("./todos.controller"), { prefix: "/todos" })
  
  f.log.info("Controllers ready")
  done()
}

module.exports = controllers
