const fp = require("fastify-plugin");
const domainClasses = require("../components/domain");


async function plugin(f, opts, done) {
  const domain = {}
  for (k in domainClasses) {
    domain[k] = new domainClasses[k](f.db.model(k));
  }
  f.decorate("domain", domain);
  
  f.log.info("Domain ready");
  done();
}

module.exports = fp(plugin);
