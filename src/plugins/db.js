const fp = require("fastify-plugin");
const mongoose = require("mongoose");
const schemas = require("../components/schemas");


async function plugin(f, opts, done) {
  const db = await mongoose.createConnection(opts.connectionString, {
    connectTimeoutMS: 5000
  }).asPromise();

  if (db.readyState != 1) {
    throw "MongoDB connection error!"
  }

  for (k in schemas) {
    db.model(k, schemas[k]);
  }
  f.decorate("db", db)

  f.log.info("Mongodb ready");

  done();
}

module.exports = fp(plugin);
