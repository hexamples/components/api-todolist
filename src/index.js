require('dotenv').config()

const f = require('fastify')({ 
  logger: {
    level: "debug",
    transport: {
      //target: "@fastify/one-line-logger",
      target: "pino-pretty"
    },
  }
})

// Los plugins tienen un timeout de registro de 10 segundos y i dont know donde cambiarlo
f.register(require('./plugins/db'), { connectionString: process.env.DB_CONNECTION_STRING })
f.register(require('./plugins/domain'))
f.register(require('./plugins/controllers'))


// Run the server!
const start = async () => {
  try {
    await f.listen({ port: 3000, host: "0.0.0.0" })
  } catch (err) {
    f.log.error(err)
    process.exit(1)
  }
}
start()
