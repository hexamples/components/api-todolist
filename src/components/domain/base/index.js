class Domain {

  constructor(model) {
    this.model = model;
  }

  async find(filters) {
    return this.model.find();
  }

  async findById(id) {
    return this.model.findById(id);
  }

  async create(data) {
    return this.model.create(data);
  }

  async deleteById(id) {
    return this.model.deleteById(id);
  }
}

module.exports.Domain = Domain
