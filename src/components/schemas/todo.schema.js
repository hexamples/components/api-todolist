const { Schema } = require("mongoose");

const Todo = new Schema({
  content: String,
  tags: Array
}, {
  timestamps: true,
});

module.exports = Todo
