FROM node:18-alpine

WORKDIR /app
ADD package.json /app/
RUN npm i

ADD . /app/

ENTRYPOINT ["npm", "run", "start"]

# docker build -t local_todo .
